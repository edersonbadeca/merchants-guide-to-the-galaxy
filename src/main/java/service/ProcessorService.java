package service;

import mapper.InputSerializer;
import mapper.OutputSerializer;
import merchant.domain.CommonMetal;
import merchant.domain.Currency;
import merchant.domain.RomanNumbers;
import merchant.domain.Transactions;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProcessorService {

    private List<RomanNumbers> romanNumbers;
    private List<Currency> currencies;
    private List<String> output;


    public ProcessorService() {
        this.output = new ArrayList<>();
        this.currencies = new ArrayList<>();
        this.romanNumbers = new ArrayList<>();
        RomanNumbers fifty = RomanNumbers.unrepeatableSymbol('L', 50);
        RomanNumbers fiveHundred = RomanNumbers.unrepeatableSymbol('D', 500);
        RomanNumbers aThousand = RomanNumbers.unrepeatableSymbol('M', 1000);
        RomanNumbers five = RomanNumbers.unrepeatableSymbol('V', 5);
        RomanNumbers aHundred = RomanNumbers.repeatableAndSubtractableSymbol('C', 100, Arrays.asList(fiveHundred, aThousand));
        RomanNumbers ten = RomanNumbers.repeatableAndSubtractableSymbol('X', 10, Arrays.asList(fifty, aHundred));
        RomanNumbers one = RomanNumbers.repeatableAndSubtractableSymbol('I',1, Arrays.asList(five, ten));

        this.romanNumbers.add(one);
        this.romanNumbers.add(five);
        this.romanNumbers.add(ten);
        this.romanNumbers.add(fifty);
        this.romanNumbers.add(aHundred);
        this.romanNumbers.add(fiveHundred);
        this.romanNumbers.add(aThousand);

    }

    public String trading(List<String> trades) {
        List<String> currencyAssignments = InputSerializer.serializeAssignments(trades, this.romanNumbers);
        this.currencies = generateCurrencies(currencyAssignments);
        ArrayList<String> onlyTrades = new ArrayList<>(trades);
        onlyTrades.removeAll(currencyAssignments);
        calculate(onlyTrades);

        return "";
    }

    private List<Currency> generateCurrencies(List<String> assignments) {
        return assignments.stream()
                .map(assignment -> {
                    String[] trade = assignment.split(" ");
                    String symbol = trade[0];
                    Character romanValue = trade[2].toCharArray()[0];
                    RomanNumbers romanNumber = this.romanNumbers.stream()
                            .filter(r -> r.getSymbol().equals(romanValue))
                            .findAny()
                            .get();
                    return new Currency(symbol, romanNumber);
                }).collect(Collectors.toList());
    }

    private void calculate(List<String> onlyTrades) {
        String noIdeaAnwser = "";
        List<CommonMetal> commonMetals = CommonMetal.commonMetalsInTransaction(this.currencies, onlyTrades);
        OutputSerializer serializer = new OutputSerializer();
        List<String> expressionsQuotes = serializer.ExpressionInTradeWithOutCredits(onlyTrades);
        List<Transactions> transactionsWithoutCredits = findTransactionsInExpressionQuote(expressionsQuotes);
        for(int i=0; i < expressionsQuotes.size();i++) {
            if(transactionsWithoutCredits.get(i).getTransactionValue().equals(0)) {
                noIdeaAnwser = "I have no idea what you are talking about";
            }else{
                System.out.println(serializer.withHowMuch(expressionsQuotes.get(i), transactionsWithoutCredits.get(i)));
            }
        }

        List<String> expressionsQuotesWithCredit = serializer.ExpressionInTradeWithCredits(onlyTrades);
        List<Transactions> transactionsWithCredits = findTransactionsInExpressionQuote(expressionsQuotesWithCredit);
        for(int i=0; i< expressionsQuotesWithCredit.size(); i++) {
            String[] piecesOfQuote = expressionsQuotesWithCredit.get(i).split(" ");
            CommonMetal metal = CommonMetal.getByComponentName(piecesOfQuote[piecesOfQuote.length-2], commonMetals);
            BigDecimal metalCredits = metal.getValuePerUnit().multiply(BigDecimal.valueOf(transactionsWithCredits.get(i).getTransactionValue()));
            System.out.println(serializer.withCredits(expressionsQuotesWithCredit.get(i), metalCredits, this.currencies));
        }
        if(!noIdeaAnwser.isEmpty()) {
            System.out.println(noIdeaAnwser);
        }

    }

    private List<Transactions> findTransactionsInExpressionQuote(List<String> quotes) {
        List<Transactions> transactions = new ArrayList<>();
        for(String quote: quotes) {
            List<String> componentsInQuotes = Arrays.asList(quote.split(" "));
            List<Currency> currencies = Currency.createTransaction(componentsInQuotes, this.currencies);
            Transactions transaction = new Transactions(currencies);
            transactions.add(transaction);
        }
        return transactions;
    }

}
