package mapper;

import merchant.domain.RomanNumbers;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InputSerializer {

    public static List<String> serialize(List<String> inputs) {
        return inputs.stream().map(input -> input.trim().replaceAll(" +", " ")
        ).collect(Collectors.toList());
    }

    public static List<String> serializeAssignments(List<String> trades, List<RomanNumbers> romanNumbers) {
        return trades.stream()
                .filter(trade -> {
                    if(trade.split(" ").length == 3) {
                        Character romanSymbol = trade.split(" ")[2].toCharArray()[0];
                        Optional<RomanNumbers> matchedSymbol = romanNumbers.stream()
                                .filter(romanNumber -> romanNumber.getSymbol().equals(romanSymbol))
                                .findAny();
                        return matchedSymbol.isPresent();
                    }
                    return false;
                }).collect(Collectors.toList());
    }

    public static List<String> serializeInput(List<String> input) {
        return input.stream()
                .filter(i -> !i.isEmpty())
                .collect(Collectors.toList());
    }
}
