package mapper;

import merchant.domain.Currency;
import merchant.domain.Transactions;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class OutputSerializer {

    public String withHowMuch(String trade, Transactions transaction) {
        String[] wordsInTrade = trade.split(" ");
        List<String> output = Arrays.asList(wordsInTrade).stream()
                .filter(t -> transaction.getTransaction().stream()
                .anyMatch(currency -> currency.getSymbol().equals(t))
                ).collect(Collectors.toList());
        output.add("is");
        output.add(transaction.getTransactionValue().toString());
        return output.stream().collect(Collectors.joining(" "));
    }

    public String withCredits(String trade, BigDecimal creditValue, List<Currency> currencies) {
        String[] wordsInTrade = trade.split(" ");
        List<String> output = Arrays.asList(wordsInTrade).stream()
                .filter(currency -> currencies.stream()
                        .anyMatch(c -> c.getSymbol().equals(currency))
                ).collect(Collectors.toList());
        output.add(wordsInTrade[wordsInTrade.length-2]);
        output.add("is");
        output.add(creditValue.stripTrailingZeros().toPlainString());
        output.add("Credits");
        return output.stream().collect(Collectors.joining(" "));
    }

    public List<String> ExpressionInTradeWithOutCredits(List<String> trades) {
        return trades.stream()
                .filter(t -> t.startsWith("how much"))
                .collect(Collectors.toList());
    }

    public List<String> ExpressionInTradeWithCredits(List<String> trades) {
        return trades.stream()
                .filter(t -> t.startsWith("how many"))
                .collect(Collectors.toList());
    }
}
