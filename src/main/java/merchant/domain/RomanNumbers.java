package merchant.domain;

import java.util.Collections;
import java.util.List;

public class RomanNumbers {
    private final Character symbol;
    private final Boolean canRepeat;
    private final Boolean canSubstract;
    private final List<RomanNumbers> subtractWith;

    public Integer getValue() {
        return value;
    }

    private final Integer value;

    public RomanNumbers(Character symbol, Boolean canRepeat,
                        Boolean canSubstract, List<RomanNumbers> subtractWith, Integer value) {
        this.symbol = symbol;
        this.canRepeat = canRepeat;
        this.canSubstract = canSubstract;
        this.subtractWith = subtractWith;
        this.value = value;
    }

    public static RomanNumbers unrepeatableSymbol(Character symbol, Integer value) {
        return new RomanNumbers(symbol, false, false, Collections.EMPTY_LIST, value);
    }


    public static RomanNumbers repeatableAndSubtractableSymbol(Character symbol, Integer value, List<RomanNumbers> subtractWith) {
        return new RomanNumbers(symbol, false, false, subtractWith, value);
    }

    @Override
    public String toString() {
        return symbol.toString();
    }

    public Character getSymbol() {
        return symbol;
    }
}
