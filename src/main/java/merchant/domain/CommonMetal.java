package merchant.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class CommonMetal {

    private String componentName;

    public BigDecimal getValuePerUnit() {
        return valuePerUnit;
    }

    private BigDecimal valuePerUnit;

    public CommonMetal(String componentName, BigDecimal valuePerUnit) {
        this.componentName = componentName;
        this.valuePerUnit = valuePerUnit;
    }


    public static List<CommonMetal> commonMetalsInTransaction(List<Currency> currencies, List<String> trades) {
        List<String> metals = tradeCredits(trades);
        List<CommonMetal> commonMetals = new ArrayList<>();
        for(String metal: metals) {
            List<Currency> currenciesForMetal = Currency
                    .createTransaction(Arrays.asList(metal.split(" ")), currencies);
            Transactions transaction = new Transactions(currenciesForMetal);
            CommonMetal listedMetal = generateMetalFromTransaction(metal, transaction);
            commonMetals.add(listedMetal);
        }
        return commonMetals;
    }

    private static List<String> tradeCredits(List<String> trades) {
        return trades.stream().filter(trade -> trade.endsWith("Credits")).collect(Collectors.toList());
    }

    private static CommonMetal generateMetalFromTransaction(String metalInTrading, Transactions transaction) {
        Integer value = transaction.getTransactionValue();
        String component = checkComponentType(metalInTrading);
        Integer creditWeight = checkCreditWeight(metalInTrading);
        BigDecimal valuePerUnit = BigDecimal.valueOf(creditWeight)
                .divide(BigDecimal.valueOf(value));
        return new CommonMetal(component, valuePerUnit);

    }
    private static String checkComponentType(String metal) {
        String[] pieces = metal.split(" ");
        for (int i = 0; i < pieces.length; i++) {
            if(pieces[i].equals("is")) {
                return pieces[i-1];
            }
        }
        return "";
    }
    private static Integer checkCreditWeight(String metal) {
        String[] pieces = metal.split(" ");
        for (int i = 0; i < pieces.length; i++) {
            if(pieces[i].equals("Credits")) {
                return Integer.parseInt(pieces[i-1]);
            }
        }
        return 0;
    }

    public static CommonMetal getByComponentName(String componentName, List<CommonMetal> commonMetals) {
        return commonMetals.stream().filter(m -> m.componentName.equals(componentName)).findFirst().get();
    }




}
