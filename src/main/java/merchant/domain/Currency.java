package merchant.domain;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Currency {
    private final String symbol;

    public RomanNumbers getRomanNumbers() {
        return romanNumbers;
    }

    private final RomanNumbers romanNumbers;

    public Currency(String symbol, RomanNumbers romanNumbers) {
        this.symbol = symbol;
        this.romanNumbers = romanNumbers;
    }

    public String getSymbol() {
        return symbol;
    }

    public Integer getRomanValue() {
        return romanNumbers.getValue();
    }

    public static List<Currency> createTransaction(List<String> transactions, List<Currency> currencies) {
        final Stream<String> items = transactions.stream()
                .filter(t -> {
                    return currencies.stream().anyMatch(c -> c.isEquals(t));
                });
        final List<Currency> CurrenciesInTransaction = items.map(galacticCurrencySymbol ->
                createTransactionByItem(galacticCurrencySymbol, currencies)).collect(Collectors.toList());
        return CurrenciesInTransaction;
    }

    public static Currency createTransactionByItem(String symbol, List<Currency> currencies) {
        return currencies.stream().filter(galacticCurrency -> galacticCurrency.isEquals(symbol)).findFirst().get();
    }

    private boolean isEquals(String symbol) {
        return this.symbol.equals(symbol);
    }
}
