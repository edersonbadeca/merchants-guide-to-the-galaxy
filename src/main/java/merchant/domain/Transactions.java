package merchant.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Transactions {
    private List<Currency> transaction;
    private Integer transactionValue;


    public Integer getTransactionValue() {
        return transactionValue;
    }

    public List<Currency> getTransaction() {
        return transaction;
    }


    public Transactions(List<Currency> transaction) {
        this.transaction = transaction;
        this.transactionValue = calculateValue();
    }

    public Integer calculateValue() {
        final List<Integer> transactionValues = this.transaction.stream()
                .mapToInt(Currency::getRomanValue)
                .boxed()
                .collect(Collectors.toList());
        List<Integer> supportCalculator = new ArrayList<>();
        for (int index = 0; index < transactionValues.size();) {
            Integer value = transactionValues.get(index);
            Integer nextValue = hasValuesToCalculate(transactionValues, index) ? 0 : transactionValues.get(index + 1);
            if (value >= nextValue) {
                supportCalculator.add(value);
                index++;
            } else {
                supportCalculator.add(nextValue - value);
                index = index + 2;
            }
        }
        return supportCalculator.stream().mapToInt(value -> value.intValue()).sum();
    }

    private Boolean hasValuesToCalculate(List<Integer> transactionValues, Integer index) {
        return index + 1 >= transactionValues.size();
    }
}
