import mapper.InputSerializer;
import service.ProcessorService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MerchantGalaxy {

    public static void main(String[] args) throws IOException {
        if(args.length < 1) {
            System.out.println("You need to pass the absolute path of the file");
            System.exit(0);
        }
        String filename = args[0];
        List<String> input = Files.lines(Paths.get(filename))
                .collect(Collectors.toList());
        input = InputSerializer.serializeInput(input);
        ProcessorService service = new ProcessorService();
        service.trading(input);
    }
}
