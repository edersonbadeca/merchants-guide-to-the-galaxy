package merchant.domain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CurrencyTest {

    @Test
    public void givenAListOfSymbols_whenCreatingATransaction_itShouldCreateCurrenciesForTheTransaction() {
        RomanNumbers fifty = RomanNumbers.unrepeatableSymbol('L', 50);
        RomanNumbers fiveHundred = RomanNumbers.unrepeatableSymbol('D', 500);
        RomanNumbers aThousand = RomanNumbers.unrepeatableSymbol('M', 1000);
        RomanNumbers five = RomanNumbers.unrepeatableSymbol('V', 5);
        RomanNumbers aHundred = RomanNumbers.repeatableAndSubtractableSymbol('C', 100, Arrays.asList(fiveHundred, aThousand));
        RomanNumbers ten = RomanNumbers.repeatableAndSubtractableSymbol('X', 10, Arrays.asList(fifty, aHundred));

        RomanNumbers one = RomanNumbers.repeatableAndSubtractableSymbol('I',1, Arrays.asList(five, ten));
        List<Currency> currencies = new ArrayList<>();
        currencies.add(new Currency("glob", one));
        currencies.add(new Currency("prok", five));
        currencies.add(new Currency("pish", ten));
        currencies.add(new Currency("tegj", fifty));

        List<String> globTransaction = Arrays.asList("glob","glob","Silver","is","34","Credits");
        List<Currency> currenciesGlobTransaction = Currency.createTransaction(globTransaction, currencies);
        assertThat(currenciesGlobTransaction.get(0).getSymbol()).isEqualTo("glob");
        assertThat(currenciesGlobTransaction.get(1).getSymbol()).isEqualTo("glob");

        List<String> globProkTransaction = Arrays.asList("glob","prok","Gold","is","57800","Credits");
        List<Currency> currenciesProkTransaction = Currency.createTransaction(globProkTransaction, currencies);
        assertThat(currenciesProkTransaction).isNotEmpty();
        assertThat(currenciesProkTransaction.get(0).getSymbol()).isEqualTo("glob");
        assertThat(currenciesProkTransaction.get(1).getSymbol()).isEqualTo("prok");
        List<String> questionOne = Arrays.asList("how", "much","is", "pish", "tegj", "glob", "glob" ,"?");
        List<Currency> currenciesInTheQuestion = Currency.createTransaction(questionOne, currencies);
        assertThat(currenciesInTheQuestion.size()).isEqualTo(4);

    }
}