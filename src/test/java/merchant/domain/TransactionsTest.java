package merchant.domain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;

public class TransactionsTest {

    @Test
    public void calculateValue() {
        RomanNumbers fifty = RomanNumbers.unrepeatableSymbol('L', 50);
        RomanNumbers fiveHundred = RomanNumbers.unrepeatableSymbol('D', 500);
        RomanNumbers aThousand = RomanNumbers.unrepeatableSymbol('M', 1000);
        RomanNumbers five = RomanNumbers.unrepeatableSymbol('V', 5);
        RomanNumbers aHundred = RomanNumbers.repeatableAndSubtractableSymbol('C', 100, Arrays.asList(fiveHundred, aThousand));
        RomanNumbers ten = RomanNumbers.repeatableAndSubtractableSymbol('X', 10, Arrays.asList(fifty, aHundred));

        RomanNumbers one = RomanNumbers.repeatableAndSubtractableSymbol('I',1, Arrays.asList(five, ten));
        List<Currency> currencies = new ArrayList<>();
        currencies.add(new Currency("glob", one));
        currencies.add(new Currency("prok", five));
        currencies.add(new Currency("pish", ten));
        currencies.add(new Currency("tegj", fifty));
        Transactions transactions = new Transactions(currencies);
        assertThat(transactions.calculateValue()).isEqualTo(44);

    }
}