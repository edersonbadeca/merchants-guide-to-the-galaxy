package merchant.domain;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class commonMetalTest {

    @Test
    public void commonMetalsInTransaction() {
        List<String> givenInput = Arrays.asList("glob is I",
                "prok is V",
                "pish is X",
                "tegj is L",
                "glob glob Silver is 34 Credits",
                "glob prok Gold is 57800 Credits",
                "how much is pish tegj glob glob ?",
                "how many Credits is glob prok Silver ?",
                "how many Credits is glob prok Gold ?",
                "how many Credits is glob prok Iron ?",
                "how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");

        RomanNumbers fifty = RomanNumbers.unrepeatableSymbol('L', 50);
        RomanNumbers fiveHundred = RomanNumbers.unrepeatableSymbol('D', 500);
        RomanNumbers aThousand = RomanNumbers.unrepeatableSymbol('M', 1000);
        RomanNumbers five = RomanNumbers.unrepeatableSymbol('V', 5);
        RomanNumbers aHundred = RomanNumbers.repeatableAndSubtractableSymbol('C', 100, Arrays.asList(fiveHundred, aThousand));
        RomanNumbers ten = RomanNumbers.repeatableAndSubtractableSymbol('X', 10, Arrays.asList(fifty, aHundred));

        RomanNumbers one = RomanNumbers.repeatableAndSubtractableSymbol('I',1, Arrays.asList(five, ten));
        List<Currency> currencies = new ArrayList<>();
        currencies.add(new Currency("glob", one));
        currencies.add(new Currency("prok", five));
        currencies.add(new Currency("pish", ten));
        currencies.add(new Currency("tegj", fifty));
        List<CommonMetal> commonMetals = CommonMetal.commonMetalsInTransaction(currencies, givenInput);
        assertThat(commonMetals).isNotEmpty();
        assertThat(commonMetals).extracting("componentName")
                .containsExactly("Silver", "Gold");
        assertThat(commonMetals).extracting("valuePerUnit")
                .containsExactly(BigDecimal.valueOf(17), BigDecimal.valueOf(14450));
    }
}