package mapper;

import merchant.domain.RomanNumbers;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class InputSerializerTest {

    @Test
    public void serialize() {
        List<String> givenInput = Arrays.asList("glob is I",
                "prok is V",
                "pish is X",
                "tegj is L",
                "glob glob Silver is 34 Credits",
                "glob prok Gold is 57800 Credits",
                "pish pish Iron is 3910 Credits",
                "how much is pish tegj glob glob ?",
                "how many Credits is glob prok Silver ?",
                "how many Credits is glob prok Gold ?",
                "how many Credits is glob prok Iron ?",
                "how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");
        List<String> expected =  InputSerializer.serialize(givenInput);
        assertThat(expected.size()).isEqualTo(12);
    }

    @Test
    public void serializeAssignments() {
        List<String> givenInput = Arrays.asList("glob is I",
                "prok is V",
                "pish is X",
                "tegj is L",
                "glob glob Silver is 34 Credits",
                "glob prok Gold is 57800 Credits",
                "pish pish Iron is 3910 Credits",
                "how much is pish tegj glob glob ?",
                "how many Credits is glob prok Silver ?",
                "how many Credits is glob prok Gold ?",
                "how many Credits is glob prok Iron ?",
                "how much wood could a woodchuck chuck if a woodchuck could chuck wood ?");
        RomanNumbers fifty = RomanNumbers.unrepeatableSymbol('L', 50);
        RomanNumbers fiveHundred = RomanNumbers.unrepeatableSymbol('D', 500);
        RomanNumbers aThousand = RomanNumbers.unrepeatableSymbol('M', 1000);
        RomanNumbers five = RomanNumbers.unrepeatableSymbol('V', 5);
        RomanNumbers aHundred = RomanNumbers.repeatableAndSubtractableSymbol('C', 100, Arrays.asList(fiveHundred, aThousand));
        RomanNumbers ten = RomanNumbers.repeatableAndSubtractableSymbol('X', 10, Arrays.asList(fifty, aHundred));
        RomanNumbers one = RomanNumbers.repeatableAndSubtractableSymbol('I',1, Arrays.asList(five, ten));
        List<RomanNumbers> romanNumbers = new ArrayList<>();

        romanNumbers.add(one);
        romanNumbers.add(five);
        romanNumbers.add(ten);
        romanNumbers.add(fifty);
        romanNumbers.add(aHundred);
        romanNumbers.add(fiveHundred);
        romanNumbers.add(aThousand);
        List<String> expected = InputSerializer.serializeAssignments(givenInput, romanNumbers);
        assertThat(expected.size()).isEqualTo(4);
    }
}