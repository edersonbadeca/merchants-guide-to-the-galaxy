package mapper;

import merchant.domain.Currency;
import merchant.domain.RomanNumbers;
import merchant.domain.Transactions;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class OutputSerializerTest {
    List<Currency> currencies;

    @Before
    public void setUp() {
        RomanNumbers fifty = RomanNumbers.unrepeatableSymbol('L', 50);
        RomanNumbers fiveHundred = RomanNumbers.unrepeatableSymbol('D', 500);
        RomanNumbers aThousand = RomanNumbers.unrepeatableSymbol('M', 1000);
        RomanNumbers five = RomanNumbers.unrepeatableSymbol('V', 5);
        RomanNumbers aHundred = RomanNumbers.repeatableAndSubtractableSymbol('C', 100, Arrays.asList(fiveHundred, aThousand));
        RomanNumbers ten = RomanNumbers.repeatableAndSubtractableSymbol('X', 10, Arrays.asList(fifty, aHundred));

        RomanNumbers one = RomanNumbers.repeatableAndSubtractableSymbol('I', 1, Arrays.asList(five, ten));
        this.currencies = new ArrayList<>();
        this.currencies.add(new Currency("glob", one));
        this.currencies.add(new Currency("prok", five));
        this.currencies.add(new Currency("pish", ten));
        this.currencies.add(new Currency("tegj", fifty));

    }

    @Test
    public void givenATransactionWithInCredits_itShouldSerializeTheCorrectValue() {
        String givenInput = "how many Credits is glob prok Silver ?";
        String expectedOutPut = "glob prok Silver is 666 Credits";
        OutputSerializer serializer = new OutputSerializer();
        List<Currency> transactions = new ArrayList();
        transactions.add(this.currencies.get(0));
        transactions.add(this.currencies.get(1));
        String output = serializer.withCredits(givenInput, BigDecimal.valueOf(666), transactions);
        assertThat(output).isEqualTo(expectedOutPut);
    }

    @Test
    public void givenATransactionWithInHowMuch_itShouldSerializeTheCorrectValue() {
        OutputSerializer serializer = new OutputSerializer();
        List<Currency> transactions = new ArrayList();
        transactions.add(this.currencies.get(2));
        transactions.add(this.currencies.get(3));
        transactions.add(this.currencies.get(0));
        transactions.add(this.currencies.get(0));
        String output = serializer.withHowMuch("how much is pish tegj glob glob ?", new Transactions(transactions));
        assertThat(output).isEqualTo("pish tegj glob glob is 42");
    }
}